package core;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class SmsMessageControllerTest {

    private final String INCOMINGSMS =
            "sms;deliver;\"Telecard\";\"\";\"\";\"2014.06.18 13:31\";\"\";\"Пароль для Интернет-платежа\"\n" +
                    "sms;deliver;\"Telecard\";\"\";\"\";\"2014.07.20 15:50\";\"\";\"233482 Пароль для проведения Интернет-платежа. Не разглашайте пароль\"\n" +
                    "sms;deliver;\"+71234323774\";\"\";\"\";\"2014.10.06 06:47\";\"\";\"Этот абонент снова в сети. Вы можете позвонить ему. \"\"Билайн\"\"\"\n";

    private final String OUTCOMINGSMS =
            "sms;submit;\"\";\"+79231643421\";\"\";\"2014.02.12 13:28\";\"\";\"Нет я на говорова\"\n" +
                    "sms;submit;\"\";\"+79231643421\";\"\";\"2014.02.14 15:02\";\"\";\"Я не успеваю сегодня\"\n" +
                    "sms;submit;\"\";\"+79231643421\";\"\";\"2014.02.12 13:22\";\"\";\"Ладно я постараюсь успеть и потом все расскажу)\"";


    private SmsMessageController smsMessageController;

    @Before
    public void initTest() {
        smsMessageController = new SmsMessageController();
    }

    @After
    public void afterTest() {
        smsMessageController = null;
    }


    @Test
    public void putLineToSmsMap() throws Exception {
        Map<String, List<SmsMessage>> smsMap = new HashMap<>();
        fillSmsMap(smsMap);
        assertEquals(smsMap.get("Telecard").get(0).getSmsText(), "Пароль для Интернет-платежа");
        assertEquals(smsMap.get("89231643421").get(1).getSmsText(), "Я не успеваю сегодня");
    }

    @Test
    public void sortSmsMap() throws Exception {
        Map<String, List<SmsMessage>> smsMap = new HashMap<>();
        fillSmsMap(smsMap);

        assertEquals(smsMap.get("Telecard").get(0).getDate(), "2014.06.18 13:31");
        assertEquals(smsMap.get("Telecard").get(1).getDate(), "2014.07.20 15:50");

        smsMessageController.sortSmsMap(smsMap); // Отсортировано по убыванию даты

        assertEquals(smsMap.get("Telecard").get(0).getDate(), "2014.07.20 15:50");
        assertEquals(smsMap.get("Telecard").get(1).getDate(), "2014.06.18 13:31");
    }

    private void fillSmsMap(Map<String, List<SmsMessage>> smsMap) {
        for (String string : INCOMINGSMS.split("\n")) {
            smsMessageController.putLineToSmsMap(string, smsMap, FileController.SourceFileType.INCOMING);
        }
        for (String string : OUTCOMINGSMS.split("\n")) {
            smsMessageController.putLineToSmsMap(string, smsMap, FileController.SourceFileType.OUTCOMING);
        }
    }

}