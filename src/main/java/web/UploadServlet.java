package web;

import core.Core;
import org.json.JSONArray;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

@WebServlet(urlPatterns = {"/upload","/contacts", "/getContacts", "/messages", "/getMessages", "/findContact", "/findMessage"})
@MultipartConfig
public class UploadServlet extends HttpServlet {

    private MessageController messageController;
    private Core core;

    @Override
    public void init() throws ServletException {
        messageController = new MessageController();
        core = new Core();
    }

    /**
     * Обработка POST запроса от формы загрузки экспортированных файлов
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getRequestURI().equals("/upload")) {
            // Создаем директорию для загрузки файлов на сервер
            String dir = "C:\\Users\\Person\\IdeaProjects\\MessageDesigner\\src\\uploads";
            File uploads = new File(dir);
            if (!uploads.exists()) uploads.mkdir();

            // Получаем файлы из запроса
            for (Part part : request.getParts()) {
                if (part.getInputStream().available() == 0)
                    continue;
                // Сохраняем полученные файлы
                File file = new File(uploads, part.getName() + ".csv");
                try (InputStream fileContent = part.getInputStream()) {
                    Files.copy(fileContent, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
            }

            // Парсим файлы, сортируем и сохраняем результат
            core.prepareMessages();

            response.sendRedirect("contacts");
        }
    }

    /**
     * Обработка GET запросов клиента
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");

        if (request.getRequestURI().equals("/contacts")) {
            request.getRequestDispatcher("contacts.jsp").forward(request, response);
        }

        // Получение списка всех имеющихся контактов
        if (request.getRequestURI().equals("/getContacts")) {
            JSONArray jsonArray = new JSONArray();
            for (String string : messageController.getListOfContacts()) {
                jsonArray.put(string);
            }
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            out.print(jsonArray);
            out.flush();
        }

        if (request.getRequestURI().equals("/messages")) {
            request.getRequestDispatcher("messages.jsp").forward(request, response);
        }

        // Получение всех сообщений с выбранным контактом
        if (request.getRequestURI().equals("/getMessages")) {
            String id = request.getParameter("id");
            JSONArray jsonArray = new JSONArray();
            for (String string : messageController.getMessages(id)) {
                jsonArray.put(string);
            }
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            out.print(jsonArray);
            out.flush();
        }

        // Поиск контакта по совпадению с введенными символами
        if (request.getRequestURI().equals("/findContact")) {
            String id = request.getParameter("id");
            JSONArray jsonArray = new JSONArray();
            for (String string : messageController.findContact(id)) {
                jsonArray.put(string);
            }
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            out.print(jsonArray);
            out.flush();
        }

        // Поиск сообщения по совпадению с введенными символами
        if (request.getRequestURI().equals("/findMessage")) {
            String id = request.getParameter("id");
            String text = request.getParameter("text");
            JSONArray jsonArray = new JSONArray();
            for (String string : messageController.findMessage(id, text)) {
                jsonArray.put(string);
            }
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();
            out.print(jsonArray);
            out.flush();
        }
    }

}