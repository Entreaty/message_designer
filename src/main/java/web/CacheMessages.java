package web;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Класс реализующий кеширование(LRU-cache) часто выполняемых запросов
 */
public class CacheMessages {

    /**
     * Hardcoded working directory path
     */
    private final static String WORKPATH = "C:\\Users\\Person\\IdeaProjects\\MessageDesigner\\src\\uploads\\results\\";

    private static final CacheMessages INSTANCE = new CacheMessages();

    private CacheMessages() {}

    public static CacheMessages getInstance() {
        return INSTANCE;
    }

    /**
     * Максимальное колличество элементов в кеше
     */
    private final int MAX_CAPACITY = 100;

    /**
     * Коллекция (контакт / список сообщений), которая пополняется при каждом новом запросе
     */
    private Map<String, List<String>> messages = new LinkedHashMap<String, List<String>>(MAX_CAPACITY, 0.75f, true){
        @Override
        protected boolean removeEldestEntry(Map.Entry<String, List<String>> eldest) {
            return size() > MAX_CAPACITY;

        }
    };
    // Список контактов
    private List<String> contacts = new ArrayList<>();

    /**
     * Возвращает список контактов
     */
    public List<String> getContacts() {
        checkContacts();
        return contacts;
    }

    /**
     * Возвращает список сообщений с конкретным контактом
     */
    public List<String> getMessagesById(String string) {
        if (messages.get(string) == null) {
            addMessagesForId(string);
        }
        return messages.get(string);
    }

    /**
     * Возвращает список контактов совпадающих с введенной строкой
     */
    public List<String> getSimilarContacts(String string) {
        checkContacts();
        List<String> results = new ArrayList<>();
        for (String item : contacts) {
            if (item.toLowerCase().contains(string.toLowerCase())) {
                results.add(item);
            }
        }
        return results;
    }

    /**
     * Возвращает список сообщений совпадающих с введенной строкой
     */
    public List<String> getSimilarMessages(String id, String text) {
        if (messages.get(id) == null) {
            addMessagesForId(id);
        }
        List<String> results = new ArrayList<>();
        for (String item : messages.get(id)) {
            if (item.toLowerCase().contains(text.toLowerCase())) {
                results.add(item);
            }
        }
        return results;
    }

    /**
     * Проверяет наличие новых контактов
     */
    private void checkContacts() {
        File folder = new File(WORKPATH);
        try {
            if (folder.listFiles().length > contacts.size()) {
                addContacts();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Сохранение контакта в кеше
     */
    private void addContacts() {
        File folder = new File(WORKPATH);
        try {
            for (final File fileEntry : folder.listFiles()) {
                contacts.add(fileEntry.getName());
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Сохранение всех сообщений контакта в кеше
     */
    private void addMessagesForId(String id) {
        List<String> list = new ArrayList<>();

        BufferedReader buffer = null;
        String currentLine;
        try {
            buffer = new BufferedReader(new FileReader(WORKPATH + id));
            while ((currentLine = buffer.readLine()) != null) {

                list.add(currentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(buffer);
        }
        messages.put(id, list);
    }
}

