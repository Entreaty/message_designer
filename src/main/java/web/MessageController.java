package web;

import java.util.List;

class MessageController {

    List<String> getMessages(String id) {
        return CacheMessages.getInstance().getMessagesById(id);
    }

    List<String> getListOfContacts(){
        return CacheMessages.getInstance().getContacts();
    }

    List<String> findContact(String string){
        return CacheMessages.getInstance().getSimilarContacts(string);
    }

    List<String > findMessage(String id, String text) {
        return CacheMessages.getInstance().getSimilarMessages(id, text);
    }
}
