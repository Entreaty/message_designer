package core;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ContactController {

    /**
     * Возвращает коллекцию мобильных контактов
     *
     * @param filePath - местоположение экспортированного файла "Контакты"
     */
    Map<String, List<Contacts>> getContactsMap(String filePath) {
        Map<String, List<Contacts>> contactsMap = new HashMap<>();
        BufferedReader buffer = null;
        try {
            String currentLine;
            buffer = new BufferedReader(new FileReader(filePath));

            while ((currentLine = buffer.readLine()) != null) {
                fillContactsMap(currentLine, contactsMap);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(buffer);
        }
        return contactsMap;
    }

    /**
     * Заполняет коллекцию с номерами контактов
     */
    protected void fillContactsMap(String currentLine, Map<String, List<Contacts>> contactsMap) {
        String[] items = currentLine.split(";");

        String mobilePhone = clearPhone(items[13]);
        String phone = clearPhone(items[14]);

        Contacts contactModel = new Contacts.Builder()
                .title(items[0])
                .name(items[1])
                .patronymic(items[2])
                .surname(items[3])
                .mobilePhone(mobilePhone)
                .phone(phone)
                .build();

        // Создаем отдельную запись для мобильного телефона
        addContact(contactsMap, mobilePhone, contactModel);

        // Создаем отдельную запись для обычного телефона,
        // нам неважно, что здесь происходит дублирование контактов
        // главное - для каждого номера иметь возможность прочитать сообщения
        addContact(contactsMap, phone, contactModel);
    }

    /**
     * Добавление в контакты данных о владельце этого номера
     */
    private void addContact(Map<String, List<Contacts>> contacts, String number, Contacts contactModel) {
        if (!number.equals("") && number.matches("[0-9]+")) {
            List<Contacts> listMobilePhone = contacts.get(number);
            if (listMobilePhone == null) {
                listMobilePhone = new ArrayList<>();
                contacts.put(number, listMobilePhone);
            }
            listMobilePhone.add(contactModel);
        }
    }

    /**
     * Изменение вида кода страны
     */
    private String clearPhone(String string) {
        return string.replace("\"", "").replace("+7", "8");
    }
}
