package core;

class Contacts {
    private final String title;
    private final String name;
    private final String patronymic;  // Отчество
    private final String surname;
    private final String mobilePhone;
    private final String phone;

    public static class Builder {
        private String title;
        private String name;
        private String patronymic;
        private String surname;
        private String mobilePhone;
        private String phone;

        public Builder title(String string) {title = string;return this;}
        public Builder name(String string) {name = string;return this;}
        public Builder patronymic(String string) {patronymic = string;return this;}
        public Builder surname(String string) {surname = string;return this;}
        public Builder mobilePhone(String string) {mobilePhone = string;return this;}
        public Builder phone(String string) {phone = string;return this;}
        public Contacts build(){return new Contacts(this);}
    }

    private Contacts(Builder builder){
        title = builder.title;
        name = builder.name;
        patronymic = builder.patronymic;
        surname = builder.surname;
        mobilePhone = builder.mobilePhone;
        phone = builder.phone;
    }

    public String getTitle() {
        return title;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getSurname() {
        return surname;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public String toString() {
        return "core.Contacts{" +
                "title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", surname='" + surname + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
