package core;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Класс отвечает за создание файлов сообщений и файла со списком всех контактов в отдельной папке
 */
class FileController {


    /**
     * Тип сообщения
     */
    public enum SourceFileType {
        INCOMING,
        OUTCOMING,
    }

    /**
     * Получает название директории, коллекцию сообщений и список контактов.
     */
    void createFiles(String directoryName, Map<String, List<SmsMessage>> sms, Map<String, List<Contacts>> contacts) {
        checkDirectory(directoryName);
        createContactsList(directoryName, contacts);
        createMessages(directoryName, sms, contacts);
    }

    /**
     * Создает файл со списком всех номеров телефонов
     */
    private void createContactsList(String directoryName, Map<String, List<Contacts>> contacts) {
        String fileName = directoryName + "/List_of_contacts.txt";
        try {
            PrintWriter writer = new PrintWriter(new FileWriter(new File(fileName), true));
            for (Map.Entry<String, List<Contacts>> contact : contacts.entrySet()) {
                writer.println(contact.toString());
            }
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly();
        }

    }

    /**
     * Создает именованные файлы(Имя_Фамилия либо Номер) содержащие все сообщения
     */
    private void createMessages(String directoryName, Map<String, List<SmsMessage>> sms, Map<String, List<Contacts>> contacts) {
        try {
            for (Map.Entry<String, List<SmsMessage>> entry : sms.entrySet()) {

                String fileName;
                if (contacts.get(entry.getKey()) != null) {
                    String a = contacts.get(entry.getKey()).get(0).getSurname();
                    String b = contacts.get(entry.getKey()).get(0).getName();
                    fileName = directoryName + "/" + clearFileName(a) + "_" + clearFileName(b) + ".txt";
                } else {
                    fileName = directoryName + "/" + clearFileName(entry.getKey()) + ".txt";
                }

                PrintWriter writer = new PrintWriter(new FileWriter(new File(fileName), true));
                for (SmsMessage str : entry.getValue()) {
                    writer.println(str.getDate() + " " + str.getType() + " " + str.getSmsText());
                }
                writer.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly();
        }
    }

    /**
     * Убирает запрещенные знаки в Windows для названия файлов
     */
    private String clearFileName(String string) {
        return string.replaceAll("[*|\\:\"<>?+]", "");
    }

    /**
     * Создает директорию, если она не существует
     */
    private void checkDirectory(String fileName) {
        if (!new File(fileName).exists())
            new File(fileName).mkdir();
    }
}
