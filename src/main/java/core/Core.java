package core;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Core {

    /**
     * Директория куда будут загружены экспортированные из телефона файлы
     */
    private static final String WORKDIR = "C:\\Users\\Person\\IdeaProjects\\MessageDesigner\\src\\uploads\\";

    public void prepareMessages() {
        
        Map<String, List<Contacts>> contacts = new ContactController().getContactsMap(WORKDIR + "CONTACTS.csv");

        Map<String, List<SmsMessage>> smsMap = new HashMap<>();

        SmsMessageController messageController = new SmsMessageController();

        // Добавление в список входящих сообщений
        messageController.fillSmsMap(smsMap, WORKDIR + "INPUTSMS.csv", FileController.SourceFileType.INCOMING);

        // Добавление в список старых сообщений
        String oldSms = WORKDIR + "OLDSMS.csv";
        if (new File(oldSms).exists()) {
            messageController.fillSmsMap(smsMap, oldSms, FileController.SourceFileType.INCOMING);
        }

        // Добавление в конец списка исходящих сообщений
        String outputSms = WORKDIR + "OUTPUTSMS.csv";
        if (new File(outputSms).exists()) {
            messageController.fillSmsMap(smsMap, outputSms, FileController.SourceFileType.OUTCOMING);
        }

        // Сортировка сообщений по дате
        messageController.sortSmsMap(smsMap);

        // Создание списка сообщений для каждого контакта
        new FileController().createFiles(WORKDIR+ "results", smsMap, contacts);
    }
}

