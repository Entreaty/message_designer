package core;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

class SmsMessageController {

    /**
     * Заполняет коллекцию сообщениями
     */
    Map<String, List<SmsMessage>> fillSmsMap(Map<String, List<SmsMessage>> smsMap, String filePath, FileController.SourceFileType sourceFileType) {
        BufferedReader buffer = null;
        String currentLine;
        try {
            buffer = new BufferedReader(new FileReader(filePath));
            while ((currentLine = buffer.readLine()) != null) {
                putLineToSmsMap(currentLine, smsMap, sourceFileType);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(buffer);
        }
        return smsMap;
    }

    /**
     * Построчное заполнение коллекции
     */
    protected void putLineToSmsMap(String currentLine, Map<String, List<SmsMessage>> smsMap, FileController.SourceFileType sourceFileType) {
        String[] parts = currentLine.split(";");

        String number = null, date, smsText;

        if (parts.length < 3) {
            return;
        } else {
            if (sourceFileType == FileController.SourceFileType.INCOMING) {
                number = removeQuotes(parts[2]).replace("+7", "8");
            } else if (sourceFileType == FileController.SourceFileType.OUTCOMING) {
                number = removeQuotes(parts[3]).replace("+7", "8");
            }
            date = removeQuotes(parts[5]);
            smsText = getSmsText(parts, currentLine);
        }
        SmsMessage smsModel = new SmsMessage(number, date, smsText, getSmsDirection(parts[1]));

        List<SmsMessage> list = smsMap.get(number);
        if (list == null) {
            list = new ArrayList<>();
            smsMap.put(number, list);
        }
        list.add(smsModel);
    }

    /**
     * Сортировка сообщений по дате
     */
    Map<String, List<SmsMessage>> sortSmsMap(Map<String, List<SmsMessage>> smsMap) {
        for (Map.Entry<String, List<SmsMessage>> entry : smsMap.entrySet()) {
            Collections.sort(entry.getValue(), new Comparator<SmsMessage>() {
                @Override
                public int compare(SmsMessage o1, SmsMessage o2) {
                    return o2.getDate().compareTo(o1.getDate());
                }
            });
        }

        return smsMap;
    }

    /**
     * Возвращает вид сообщения - входящее/исходящее
     */
    private SmsMessage.Delivery getSmsDirection(String part) {
        return (part.equals("submit")) ? SmsMessage.Delivery.OUT : SmsMessage.Delivery.IN;
    }

    /**
     * Получение текста собщений
     */
    private String getSmsText(String[] parts, String currentLine) {
        String smsText = null;
        if (parts.length > 3 && parts.length < 8) {
            smsText = removeQuotes(parts[6]);
        } else if (parts.length == 8) {
            smsText = removeQuotes(parts[7]);
        } else if (parts.length > 8) {
            smsText = currentLine.replace("\"\"", "'");
            smsText = smsText.split("\"")[5];
        }
        return smsText;
    }

    /**
     * Удаление ненужных кавычек
     */
    private String removeQuotes(String part) {
        return part.substring(1, part.length() - 1);
    }

}
