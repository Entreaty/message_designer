package core;

class SmsMessage {
    private String number;
    private String date;
    private String smsText;
    private Delivery type;

    public enum Delivery{
        IN,
        OUT
    }

    public SmsMessage(String number, String date, String smsText, Delivery type) {
        this.number = number;
        this.date = date;
        this.smsText = smsText;
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public String getDate() {
        return date;
    }

    public String getSmsText() {
        return smsText;
    }

    public Delivery getType() {
        return type;
    }

    @Override
    public String toString() {
        return "core.SmsMessage{" +
                "number='" + number + '\'' +
                ", date='" + date + '\'' +
                ", smsText='" + smsText + '\'' +
                ", type=" + type +
                '}';
    }
}
