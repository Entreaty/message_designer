<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title> Messages </title>
    <%--jQuery--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
<h3>Поиск сообщения</h3>
<label for="find_message">По сообщению </label><input type="text" id="find_message">
<h3>Сообщения с контактом <span id="contact"><%= request.getParameter("id")%></span></h3>
<div id="results"></div>

<script>
    // Получает список всех сообщений для данного контакта
    var full_messages = null;
    var id = $('#contact')[0].innerHTML;
    $.ajax({
        type: "GET",
        url: "/getMessages",
        data: "id=" + id,
        success: function (data) {
            $('#results').html('');
            if (data) {
                data.forEach(function (item, i, arr) {
                    $('#results').append("<p>" + item + "</p>");
                });
                if (full_messages == null)full_messages = $('#results')[0].innerHTML;
            }
        }
    });

    // Поиск по сообщениям
    $('#find_message').keyup(function (eventObject) {
        var item = $('#find_message');
        if (item.val().length > 1) {
            var value = item.val();
            findMessage(value)
        } else if (item.val().length <= 1) {
            $('#results').html(full_messages);
        }
    });
    function findMessage(string) {
        $.ajax({
            type: "GET",
            url: "/findMessage",
            data: "id=" + id + "&text=" + string,
            success: function (data) {
                if (data) {
                    $('#results').html('');
                    data.forEach(function (item, i, arr) {
                        $('#results').append("<p>" + item + "</p>");
                    });
                }
            }
        });
    }

</script>
</body>
</html>
