<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>File Uploading Form</title>
    <style>
        .label{
            width: 220px;
            display: inline-block;
        }
    </style>
</head>
<body>
<h3>File Upload:</h3>
Выберете файлы для загрузки: <br/>
<form action="upload" method="post" enctype="multipart/form-data">
    <label class="label" for="file_contacts" >Контакты: </label><input id="file_contacts" type="file" name="CONTACTS" size="50"/>
    <br>
    <label class="label" for="file_input_sms">Входящие сообщения: </label><input id="file_input_sms" type="file" name="INPUTSMS" size="50"/>
    <br>
    <label class="label" for="file_output_sms">Исходящие сообщения: </label><input id="file_output_sms" type="file" name="OUTPUTSMS" size="50"/>
    <br>
    <label class="label" for="file_old_sms">Сохраненные сообщения: </label><input id="file_old_sms" type="file" name="OLDSMS" size="50"/>
    <br><br>
    <input type="submit" value="Загрузить"/>
</form>
</body>
</html>