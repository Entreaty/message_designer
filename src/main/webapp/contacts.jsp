<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>File Uploading Form</title>

    <%--jQuery--%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
<h3>Поиск контакта</h3>
<label for="find_contact">По названию </label><input type="text" id="find_contact">
<div id="results"></div>

<script>
    // Получает список всех контактов
    var full_contacts = null;
    $.ajax({
        type: "GET",
        url: "/getContacts",
        dataType: "script",
        success: function (data) {
            $('#results').html('');
            if (data) {
                JSON.parse(data).forEach(function (item, i, arr) {
                    $('#results').append("<a href='messages?id=" + item + "'>" + item + "</a><br>");
                });
                if (full_contacts == null)full_contacts = $('#results')[0].innerHTML;
            }
        }
    });

    // Поиск контактов по 2 и более введенным символам
    $('#find_contact').keyup(function (eventObject) {
        var item = $('#find_contact');
        if (item.val().length > 1) {
            var value = item.val();
            findContact(value)
        } else if (item.val().length <= 1) {
            $('#results').html(full_contacts);
        }
    });

    // Поиск контактов по введенным символам
    function findContact(id) {
        $.ajax({
            type: "GET",
            url: "/findContact",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    $('#results').html('');
                    data.forEach(function (item, i, arr) {
                        $('#results').append("<a href='messages?id=" + item + "'>" + item + "</a><br>");
                    });
                }
            }
        });
    }

</script>

</body>
</html>